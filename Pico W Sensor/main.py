# Import libraries
from machine import unique_id, Pin, I2C, Timer
from microdot_asyncio import Microdot, send_file

import utime
import network

import ahtx0
from sgp40 import SGP40

# Status Led
status_pin = Pin(9, Pin.OUT)
is_setup = False
status_led_toggle = lambda _: (status_pin.value(not status_pin.value()) if not is_setup else False)
soft_timer = Timer(mode=Timer.PERIODIC, period=700, callback=status_led_toggle)

# UV LEDS
button_pin = Pin(2, Pin.IN, Pin.PULL_UP)
uv_out_pin = Pin(18, Pin.OUT)
uv_out_pin.value(0)

last_change = 0

def uv_callback(p):
    print('Pin change', p)
    # toggle uv_out_pin
    # only toggle if last change was more than 0.3 seconds ago
    global last_change
    if utime.ticks_ms() - last_change > 300:
        last_change = utime.ticks_ms()
        uv_out_pin.value(not uv_out_pin.value())

button_pin.irq(trigger=Pin.IRQ_FALLING, handler=uv_callback)


# Air Quality Sensor
i2cbus = I2C(1, scl=Pin(15), sda=Pin(14))
sgp40 = SGP40(i2cbus, 0x59)

air_timer = None

# Allow the sensor to warm up
def air_qual_callback(p):
    index = sgp40.measure_index()
    if index > 0:
        status_pin.value(0)
        air_timer.deinit()
    print(index)

    
air_timer = Timer(mode=Timer.PERIODIC, period=1000, callback=air_qual_callback)

    
# Temperature and Humidity Sensor
i2c = I2C(0, scl=Pin(17), sda=Pin(16))
aht10 = ahtx0.AHT10(i2c)


# Web Server
app = Microdot()

ssid = "Flatwise"
password = "123456789"

@app.route("/")
async def index(request):
    return send_file("/static/index.html")

@app.get("/get/vals")
async def values(request):
    ''' Request values from the sensors '''
    sensor_temperature = aht10.temperature
    calibrated_temperature = 0.56 * sensor_temperature + 2.375
    index = sgp40.measure_index_avg(int(aht10.relative_humidity), calibrated_temperature, 2, 200)
    return {
        "humidity": aht10.relative_humidity, # type: ignore
        "temp": calibrated_temperature, # type: ignore
        "voc_index": index, # type: ignore
    }

@app.get('/shutdown')
async def shutdown(request):
    request.app.shutdown()
    return 'The server is shutting down...'

@app.route('/<path:path>')
def static(request, path):
    if '..' in path:
        # directory traversal is not allowed
        return 'Not found', 404
    return send_file('static/' + path, max_age=86400)

# Setup Wifi
ap = network.WLAN(network.AP_IF)
ap.config(essid=ssid, password=password)
ap.active(True)

while ap.active == False:
    pass

# Set Status LED to indicate setup is complete
is_setup = True
status_pin.value(1)

# Run
app.run(port=80, debug=True)
