
import numpy as np
import matplotlib.pyplot as plt

sensor_data = np.array([28.376, 16, 12.5])
real_data = np.array([17.8, 13, 8])

# do linear regression
slope, intercept = np.polyfit(sensor_data, real_data, 1)
x = np.linspace(sensor_data.min(), sensor_data.max(), 100)

print('slope: ', slope)
print('intercept: ', intercept)
print('equation: real_data = ', slope, 'sensor_data + ', intercept)

# plot with real_data on the y-axis
fig, ax = plt.subplots()
ax.plot(sensor_data, real_data, 'o')
ax.plot(x, slope*x + intercept)
ax.set_ylabel('Real Data')
ax.set_xlabel('Sensor Data')
ax.set_title('Sensor Calibration')
plt.show()

