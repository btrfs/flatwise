const API_KEY = "f93987f7b14b467abb082139231010"

const IDEAL_TEMP = 19.5
const IDEAL_HUMIDITY = 45

function infmapto1(x, d=10) {
    // Shifted 1/x function
    if (x < 0) return 0
    return 1/(1+x/d)    
}


function calcScoreWithOutside(inside, outside, ideal, size_factor=1, diff_threshold=5) {
    // Flatwise score calculation
    let diff_in = Math.abs(inside - ideal)
    let diff_out = Math.abs(outside - ideal)
    let diff_ratio = diff_in / diff_out
    if (diff_out < diff_threshold) {
        return infmapto1(diff_in, d=4 * size_factor)
    } else {
        return infmapto1(diff_ratio, d=0.5)
    }
}

// 
function computeScore(inside_temp, outside_temp, inside_humidity, outside_humidity, voc_index) {
    // Calculate the temperature score
    let temp_score = calcScoreWithOutside(inside_temp, outside_temp, IDEAL_TEMP)

    // Calculate the humidity score
    let humidity_score = calcScoreWithOutside(inside_humidity, outside_humidity, IDEAL_HUMIDITY, size_factor=6, diff_threshold=20)

    // Calculate the VOC score
    let voc_score = voc_index < 50 ? 1 : 1 - (voc_index - 50) / 350
    if (voc_score < 0) voc_score = 0

    // Calculate the mean of the scores
    let total_score = (temp_score + humidity_score + voc_score) / 3 * 10

    return [total_score, temp_score * 10, humidity_score * 10, voc_score * 10]
}

function getSensorVals() {
    // Request Sensor Data from the server
    return new Promise((resolve, reject) => {
        $.get(`/get/vals`, (data) => {
            resolve(data.temp, data.humidity, data.qual);
        })
    })
}

function getWeatherAtQuery(q) {
    return new Promise((resolve, reject) => {
        $.get(`http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${q}&aqi=no`, (data) => {
            resolve([data.current.temp_c, data.current.humidity, data.location.name + ", " + data.location.country])
        })
    })
}
 

window.onload = (() => {
    setTimeout(update, 1 * 1000)
})

$(document).ready(() => {

    $("button#rate-button").click(async () => {
        // City input guard
        city = $("#city-input").val()
        if (!city) {
            alert("Please provide a city to evaluate data")
            return;
        }

        try {

            $("#rate-button").addClass("is-loading");

            // Get data from the Weather API
            let [outside_temp, outside_humidity, location] = await getWeatherAtQuery(city)
            let temp, humidity, voc_index;
            [temp, humidity, voc_index] = await new Promise((resolve, reject) => {
                $.getJSON('/get/vals',  (data) => {
                    resolve([data.temp, data.humidity, data.voc_index])
                }, (err) => reject(err))
            })

            // Calculate the score
            let overall_score, temp_score, humidity_score, voc_score;
            [overall_score, temp_score, humidity_score, voc_score] = computeScore(temp, outside_temp, humidity, outside_humidity, voc_index)

            // Update the UI
            $("#rate-button").removeClass("is-loading");
            $(".form").hide()

            $("#location-text").text(`Current Weather Data from ${location} at ${new Date().toLocaleTimeString()}`)
            $("#rating-container").show()

            $("#score").text(overall_score.toFixed(1));
            $("#humidity-score").text(humidity_score.toFixed(1));
            $("#humidity-text").text(`Outside Humidity: ${outside_humidity.toFixed(1)}%, Inside Humidity: ${humidity.toFixed(1)}%`)
            $("#temp-score").text(temp_score.toFixed(1));
            $("#temp-text").html('Outside Temperature: ' + outside_temp.toFixed(1) + '&deg;C, Inside Temperature: ' + temp.toFixed(1) + '&deg;C')
            $("#voc-score").text(voc_score.toFixed(1));
            $("#voc-text").text(`VOC Index: ${voc_index}`)
        } catch (e) {
            // If there is an error, show it to the user
            $("#rate-button").removeClass("is-loading");
            $("#rate-button").addClass("is-danger");
            alert(e)
        }
   })
})